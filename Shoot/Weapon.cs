﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shoot
{
    public abstract class Weapon:IDisplayable
    {
        public string Name;
        public static int count=0;
        /// <summary>
        /// Стрельба из оружия
        /// </summary>
        public abstract void Shoot();
        public virtual void DisplayName()
        {

        }
        public abstract void Display();
    }
    public interface IDisplayable
    {
        void Display();
    }
}
