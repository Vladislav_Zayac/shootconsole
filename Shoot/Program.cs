﻿using System;

namespace Shoot
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player("Artur", new Pistol("Glock"));
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey();
                Console.Clear();
                switch (key.Key)
                {
                    case ConsoleKey.D0:
                        player.SetWeapon(null);
                        player.WriteCount();
                        break;
                    case ConsoleKey.D1:
                        player.SetWeapon(new Pistol());
                        break;
                    case ConsoleKey.D2:
                        player.SetWeapon(new MachineGun());
                        break;
                    case ConsoleKey.Spacebar:
                        player.UseWeapon();
                        break;
                    case ConsoleKey.Escape:
                        break;
                }
            }
            while (key.Key != ConsoleKey.Escape);
        }
    }
}
