﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shoot
{
    public class MachineGun: Weapon
    {        
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public MachineGun()
        {
            Name = "AK";
            count++;
        }
        /// <summary>
        /// Конструктор с параметром
        /// </summary>
        /// <param name="name">Название оружия</param>
        public MachineGun(string name)
        {
            this.Name = name;
        }
        /// <summary>
        /// Стрельба из оружия
        /// </summary>
        public override void Shoot()
        {
            Console.WriteLine($"MachineGun {Name} make tra ta ta ta ta!");
        }
        public override void DisplayName()
        {
            Console.WriteLine(this.Name);
        }

        public override void Display()
        {
            throw new NotImplementedException();
        }
    }
}
