﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shoot
{
    public sealed class Pistol:Weapon
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public Pistol()
        {
            Name = "Makarov";
            count++;
        }
        /// <summary>
        /// Конструктор с параметром
        /// </summary>
        /// <param name="name">Название оружия</param>
        public Pistol(string name)
        {
            Name = name;
        }
        /// <summary>
        /// Стрельба из оружия
        /// </summary>
        public override void Shoot()
        {
            Console.WriteLine($"Pistol {Name} make paw paw!");
        }
        public override void DisplayName()
        {
            Console.WriteLine(this.Name);
        }

        public override void Display()
        {
            throw new NotImplementedException();
        }
    }
}
