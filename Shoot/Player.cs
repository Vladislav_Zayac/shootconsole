﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shoot
{
    public class Player
    {
        private string Name;
        private Weapon Gun;
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public Player()
        {
            Name = "Lexa";
            Gun = null;            
        }

        public void WriteCount()
        {
            Console.WriteLine(Weapon.count);
        }

        /// <summary>
        /// Конструктор с параметрами
        /// </summary>
        /// <param name="name">Имя игрока</param>
        /// <param name="gun">Оружие игрока</param>
        public Player(string name, Weapon gun)
        {
            this.Name = name;
            this.Gun = gun;
        }
        /// <summary>
        /// Стрельба из оружия
        /// </summary>
        public void UseWeapon()
        {
            if (Gun!=null)
            {
                Console.WriteLine($"{Name} use gun!");
                Gun.Shoot();
            }
            else
            {
                Console.WriteLine($"{Name} haven't gun!");
            }
            
        }
        /// <summary>
        /// Взять или выбросить оружие
        /// </summary>
        /// <param name="gun">Оружие</param>
        public void SetWeapon(Weapon gun)
        { 
            if(gun==null)
            {
                Console.WriteLine($"{Name} quit gun!");
                this.Gun = gun;
            }
            else
            {
                Console.WriteLine($"{Name} put gun {gun.Name}!");
                this.Gun = gun;
            }       
            if(gun is Pistol)//проверка типа данных
            {
                Pistol pistol = Gun as Pistol;
            }
            
        }
    }
}
